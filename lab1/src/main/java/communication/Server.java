package communication;

import java.net.*;
import java.io.*;
import java.util.HashMap;

public class Server {
    private int port;
    private ServerSocket serverSocket;
    private PrintWriter out;
    private BufferedReader in;
    private HashMap<String, String> DNStoIP;

    public Server(int port) {
        this.port = port;
    }

    private void start(){
        DNStoIP = new HashMap<>();
        try (ServerSocket serverSocket = new ServerSocket(port)){
            while (true){
                try {
                    Socket clientSocket = serverSocket.accept();
                    new Thread(() -> handleClient(clientSocket)).start();
                } catch (IOException ignored){
                    System.err.println("Error accepting the client");
                }
            }

        } catch (IOException ignored){
            System.err.println("Could not start the server socket");
        }
    }

    private void handleClient(Socket clientSocket) {
        try{
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            String req = in.readLine();
            String method = req.split(" ")[0];
            String response = "";
            System.out.println("Server:" + req);

            if (method.equals("REGISTER")){
                response = register(req);
            } else if (method.equals("LOOKUP")){
                response = lookup(req);
            } else {
                System.out.println("Unknown method");
            }
            out.println(response);
            clientSocket.close();
            out.close();
            in.close();
        } catch (IOException ignored){
            System.err.println("Error handling client");
        }
    }

    private String register(String req) {
        String DNSname = req.split(" ")[1];
        String IPaddr = req.split(" ")[2];
        if (DNStoIP.containsKey(DNSname)){
            return "-1";
        }
        DNStoIP.put(DNSname,IPaddr);
        return "1\n" + DNSname + " " + IPaddr;
    }

    private String lookup(String req) {
        String DNSname = req.split(" ")[1];
        if (DNStoIP.containsKey(DNSname)){
            return "1\n" + DNSname + " " + DNStoIP.get(DNSname);
        }
        return "-1";
    }

    private void stop(){
        try{
            serverSocket.close();
        }
        catch (IOException ignored){
        }
    }

    public static void main(String[] args) {
        Server server = new Server(Integer.parseInt(args[0]));
        server.start();
    }
}
