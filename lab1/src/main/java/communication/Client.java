package communication;

import java.net.*;
import java.io.*;
import java.util.Arrays;

public class Client {
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;


    private void start(String[] args) {
        String msg = String.join(" ", Arrays.copyOfRange(args,2, args.length));
        try{
            clientSocket = new Socket(args[0], Integer.parseInt(args[1]));
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            out.println(msg);
            System.out.print("Client:" + msg + " : ");
            in.lines().forEach( line -> System.out.print(line + " "));
        } catch (IOException ignored){
            System.out.print(msg + ":" + "ERROR");
        } finally {
            try{
                in.close();
                out.close();
                clientSocket.close();
            } catch (Exception ignored){

            }
        }

    }

    public static void main (String args[]) {
        Client client = new Client();
        client.start(args);
    }
}
