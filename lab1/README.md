<h2 id="task">Task for lab1</h2>

<h3 id="the-application"><i class="icon-pencil"></i> The Application</h3>

<p>In this assignment you will write a client-server application to resolve DNS-like names, i.e. to retrieve the IP address corresponding to a DNS name.</p>

<p>Thus, the server must support the following operations:</p>

<dl>
<dt>register</dt>
<dd>to bind a DNS name to an IP address. It returns -1, if the name has already been registered, and the number of bindings in the service, otherwise</dd>

<dt>lookup</dt>
<dd>to retrieve the IP address previously bound to a DNS name. It returns the IP address in the dotted decimal format or the NOT_FOUND string, if the name was not previously registered.</dd>
</dl>

<blockquote>
  <p>IMP.- Your service is not supposed to use DNS to respond to a lookup request. Instead, it should keep a “table” with pairs (DNS name, IP address), which is looked up when processing the lookup request. The processing of a register request should add a (DNS name, IP address) pair to that “table”.</p>
</blockquote>



<h3 id="client-server-application-level-protocol"><i class="icon-file"></i> Client-server application-level protocol</h3>

<p>In a client-server application, the client sends requests to the server. The latter processes the requests, returning the results, if any, to the clients.</p>

<p>In an application based on sockets both the requests and the results are sent in messages. The message sent by the client should include the operation and its parameters, if any. It is the client’s responsibility to build the request message. The server will have to extract the operation and the parameters from each message received from a client, and then invoke the function performing the requested operation. Furthermore, if the request elicits a reply message, the server will have to build it , possibly including the results of processing the request. The client in turn will have to interpret the reply message, if any.</p>

<p>The format and meaning of messages exchanged between the client and server, and the rules that govern the exchange of these messages makes the communication protocol between the client and server.</p>

<blockquote>
  <p>In this work, the server must be able to reply to two distinct requests:</p>
  
  <ul>
  <li>registration of plate number and its owner</li>
  <li>query of the owner of given plate number</li>
  </ul>
</blockquote>

<p>A possible format for the messages the client will send to the server is:</p>

<p><strong>REGISTER &lt;DNS name&gt; &lt;IP address&gt;</strong></p>

<p><strong>LOOKUP &lt;DNS name&gt;</strong></p>

<p>where</p>

<dl>
<dt>&lt;DNS name&gt;</dt>
<dd>is a DNS-like name, e.g. www.fe.up.pt</dd>

<dt>&lt;IP address&gt;</dt>
<dd>is an IPv4 address in dotted decimal format, e.g. 10.227.240.205</dd>
</dl>

<p>A possible format for the reply messages by the server to these requests is, respectively:</p>

<blockquote>
  <p><strong>&lt;result&gt;</strong> <br>
  <strong>&lt;DNS name&gt; &lt;IP address&gt;</strong></p>
</blockquote>

<p>where</p>

<dl>
<dt><strong>&lt;result&gt;</strong></dt>
<dd>is the return value (an integer) of the register command: return -1 if the command fails; otherwise, returns the number of DNS names previously registered in the service.</dd>
</dl>

<blockquote>
  <p>Note: To simplify the communication, each message, request or reply, should be a single string.</p>
</blockquote>



<h3 id="unicast-communication-with-javas-datagramsocket"><i class="icon-file"></i> Unicast Communication with Java’s DatagramSocket</h3>

<p>In this lab you shall implement two classes, Client and Server, for the client and server, respectively, using the <em>java.net.DatagramSocket</em> class.</p>



<h4 id="server">Server</h4>

<p>The server program shall be invoked as follows:</p>

<blockquote>
  <p><strong>java Server &lt;port number&gt; </strong></p>
</blockquote>

<p>where</p>

<dl>
<dt>&lt;port number&gt;</dt>
<dd>is the port number the server shall use to provide the service</dd>
</dl>

<p>The server must loop forever waiting for client requests, processing and replying to them.</p>

<p>To trace the operation of the server, it should print a messages on its terminal each time it processes a client request. The format of this message shall be:</p>

<blockquote>
  <p><strong>Server: &lt;oper&gt; &lt;opnd&gt;*</strong></p>
</blockquote>

<p>where</p>

<dl>
<dt>&lt;oper&gt;</dt>
<dd>is operation received on the request</dd>

<dt>&lt;opnd&gt;*</dt>
<dd>is the list of operands receive in the request</dd>
</dl>



<h3 id="client">Client</h3>

<p>The client program shall be invoked as follows:</p>

<blockquote>
  <p><strong>java Client &lt;host&gt; &lt;port&gt; &lt;oper&gt; &lt;opnd&gt;*</strong></p>
</blockquote>

<p>where</p>

<dl>
<dt>&lt;host&gt;</dt>
<dd>is the DNS name (or the IP address, in the dotted decimal format) where the server is running</dd>

<dt>&lt;port&gt;</dt>
<dd>is the port number where the server is providing service</dd>

<dt>&lt;oper&gt;</dt>
<dd>is the operation to request from the server, either “register” or “lookup”</dd>

<dt>&lt;opnd&gt;*</dt>
<dd>is the list of operands of that operation <br>
<strong>&lt;DNS name&gt; &lt;IP address&gt;</strong> for <strong>register</strong> <br>
<strong>&lt;DNS name&gt;</strong> for <strong>lookup</strong></dd>
</dl>

<p>After submitting a request, the client waits to receive a reply to the request, prints the reply, and then terminates.</p>

<p>The client should print a messages on its terminal to check the operation of the service. The format of this message shall be:</p>

<blockquote>
  <p><strong>Client: &lt;oper&gt; &lt;opnd&gt;* : &lt;result&gt;</strong></p>
</blockquote>

<p>where</p>

<dl>
<dt>&lt;oper&gt;</dt>
<dd>is operation requested</dd>

<dt>&lt;opnd&gt;*</dt>
<dd>is the list of operands of the request</dd>

<dt>&lt;result&gt;</dt>
<dd>is result returned by the server or “ERROR”, if an error occurs</dd>
</dl>